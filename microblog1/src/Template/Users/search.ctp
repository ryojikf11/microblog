<?php
$this->assign('title','search');
?>
<div class = "container">
    <h1>search</h1>
    <?= $this->Form->create('User',['action' =>'search']); ?>
    <div class="form-group">
    <?= $this->Form->input('username',['class'=>"form-control w-50"]); ?>
    </div>
    <?= $this->Form->button('search',["class"=>"btn btn-primary"]); ?>
    <?= $this->Form->end(); ?>
    <?= $this->Form->create('Posts', [ "action" => "search" ]); ?>
    <div class="form-group">
    <?= $this->Form->input('content',['class'=>"form-control w-50"]); ?>
    </div>
    <?= $this->Form->button('search',["class"=>"btn btn-primary"]); ?>
    <?= $this->Form->end(); ?>

<?= $this->Form->create(null, ['url' =>['action' => 'page']]); ?>
<?php if(isset($users)) :?>
    <section class="bg-light text-center py-5 mt-2">
        <h2 class="mb-5">Users</h2>
          <div class="container">
            <table class="table">
              <thead>
                <tr><th>profile_image</th><th>username</th></tr>
              </thead>
              <tbody>
                  <?php foreach($users as $user) :?>
                  <tr><td><?php if($user['image'] !==null) :?><img src="/../../../<?= h($user['image']) ;?>"><?php else :?><?= h('no image') ?><?php endif;?></div></td>
                      <td><?= $this->Html->link($user['username'],['action' => 'profile', $user->id]); ?></td></tr>
                  <?php endforeach ;?>
              </tbody>
            </table>
          </div>
    </section>
<?php endif ;?>
<?= $this->Form->end(); ?>
<?php if(isset($posts)) :?>
    <?php foreach($posts as $post) :?>
        <div class="card m-auto shadow" style="width: 50rem;">
            <div class="align-self-start  d-flex">
                <?php if($post->profile !== null) :?>
                    <div class=""><img src="/../../../<?= h($post->profile) ?>" id ="profile"></div>
                <?php else :?>
                    <div class=""><img src="/../../../microblog/microblog1/webroot/img/noprofile.jpg" style="height:75px;" id ="profile"></div>
                <?php endif ; ?>
                <div class=""><h6><?= h($post->username); ?></h6></div>
            </div>
            <div class="justify-content-center text-center">
                <div class="pb-3"><?= h($post->content); ?></div>
                <?php if($post->picture !== null) :?>
                    <div class="in-pict mb-5">
                        <img src="/../../../<?= h($post->picture) ;?>">
                    </div>
                <?php endif;?>
            </div>
    <?php endforeach ;?>
<?php endif ;?>
