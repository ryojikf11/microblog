<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <title>
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->css('styles.css') ?>

</head>
<body>
    <?= $this->element('my_header') ?>
    <?= $this->Flash->render() ?>
    <?= $this->Flash->render('auth') ?>
    <section class = "container">
        <?= $this->fetch('content')  ?>
    </section>
</body>
</html>
