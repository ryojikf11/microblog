<?php
$this->assign('title', 'Edit Post') ;
?>

<h1>
    <?= $this->Html->link('Back',['action'=>'index']); ?>
    Edit Post
</h1>

<?= $this->Form->create($post); ?>
<?= $this->Form->input('body',['rows'=>'3']); ?>
<?= $this->Form->button('Update'); ?>
<?= $this->Form->end(); ?>
