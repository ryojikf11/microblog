<?php
$this->assign('title', 'Post Creation') ;
?>

<h1>
    <?= $this->Html->link('Back',['action'=>'index']); ?>
    Post
</h1>

<?= $this->Form->create($post, ['type' => 'file']); ?>
<?= $this->Form->input('content',['rows'=>'5']); ?>
<?= $this->Form->input('image',['type' => "file"]); ?>
<?= $this->Form->button('Add'); ?>
<?= $this->Form->end(); ?>
