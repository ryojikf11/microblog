<?php
$this->assign('title','Mainpage');
$me = $this->request->getSession()->read('Auth.User');
?>


<h1>
    <?= $this->Html->link('Add New',['action'=>'add']); ?>
    Mainpage
</h1>
    <p><?= $me['username']; ?> is active</p>
    <?php echo $this->Html->link('logout',[
        'controller' => 'Users',
        'action' => 'logout'
        ]) ?>

<ul>
    <?php foreach ($posts as $post) :?>
        <li>
            <?= h($post->content); ?>
            <?php if($post['user_id'] == $me['user_id']) :?>
                <?= $this->Html->link('[Edit]',['action' => 'edit', $post->id]); ?>
                <?=
                    $this->Form->postLink(
                        '[x]',
                        ['action'=>'delete', $post->id],
                        ['confirm' => 'Are you sure?']
                    );
                ?>
            <?php endif ;?>
        </li>
    <?php endforeach ;?>
</ul>
