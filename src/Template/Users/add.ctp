<?php
$this->assign('title','register');
?>

<h1>Register</h1>
<?= $this->Form->create(null, [
    'url' =>['controller' => 'Users', 'action' => 'add']
]); ?>
<?= $this->Form->input('username'); ?>
<?= $this->Form->input('email'); ?>
<?= $this->Form->input('password'); ?>
<?= $this->Form->input('password_confirm',array(
    'type' => 'password'
)); ?>
<?= $this->Form->button('Add'); ?>
<?= $this->Form->end(); ?>
<?php echo $this->Html->link('login',[
    'controller' => 'Users',
    'action' => 'login'
    ]) ?>
