<?php

// /posts/index
// /posts
// /(controller)/(action)/(options)

namespace App\Controller;

class PostsController extends AppController
{
    public function index()
    {
        if (!$this->request->session()->check('userinfo')){
            // return $this->redirect(['controller' => 'Users','action' => 'login']);
        }
        $posts = $this->Posts->find('all');
        $this->set('posts',$posts);
    }
    public function view($id = null)
    {
        // $post = $this->Posts->get($id);
        $post = $this->Posts->get($id, [
          'contain' => 'Comments'
          ]);
    }
    public function add()
    {
        $post = $this->Posts->newEntity();
        if ($this->request->is('post')) {
            $post->user_id = $this->Auth->user('user_id');
            if($this->request->data['image']['name'] !== ""){
                $imagepath = $this->upload_image($this->request->data['image']);
                $post->image_path = $imagepath;
            }
            $post = $this->Posts->patchEntity($post,$this->request->data);
            if ($this->Posts->save($post)) {
                $this->Flash->success('Add Success');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('Add Error');
                $this->log(print_r($post->errors(),true),LOG_DEBUG);
                //error
            }
        }
        $this->set('post',$post);
    }
    public function edit($id = null)
    {
        $post = $this->Posts->get($id);
        if ($this->request->is(['post', 'patch', 'put'])) {
            $post = $this->Posts->patchEntity($post, $this->request->data);
            if ($this->Posts->save($post)) {
                $this->Flash->success('Edit Success');
                return $this->redirect(['action' => 'index']);
            } else {
                //error
                $this->Flash->error('Edit Error');
            }
        }
        $this->set(compact('post'));

    }
    public function delete($id = null)
    {
        $this->request->allowMethod(['post','delete']);
        $post = $this->Posts->get($id);
        if ($this->Posts->delete($post)) {
            $this->Flash->success('Delete Success');
        } else {
            $this->Flash->error('Delete Error');
        }
        return $this->redirect(['action' => 'index']);
    }
}
