<?php

namespace App\Controller;

class UsersController extends AppController
{
    public function login()
    {
        if ($this->request->session()->check('userinfo')){
            return $this->redirect(['controller' => 'Posts','action' => 'index']);
        }
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            // pr($user);
            // die;
            if ($user) {
                $this->Auth->setUser($user);
                $this->request->session()->write('userinfo', $user);
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error('your username or password is not correct');
        }
    }
    public function logout()
    {
        $this->Flash->success('logout');
        $this->request->session()->destroy();
        return $this->redirect($this->Auth->logout());
    }
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user,$this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success('User Registration Success');
                return $this->redirect(['action' => 'login']);
            } else {
                $this->Flash->error('User Registration Error');
                //error
            }
        }
        $this->set('user',$user);
    }
    public function initizalize()
    {
        parent::initizalize();
        $this->Auth->allow(['logout','add','login']);
    }
    // public function view($id = null)
    // {
    //     // $post = $this->Posts->get($id);
    //     $post = $this->Posts->get($id, [
    //       'contain' => 'Comments'
    //       ]);
    // }
    // public function add()
    // {
    //     $post = $this->Posts->newEntity();
    //     if ($this->request->is('post')) {
    //         $post = $this->Posts->patchEntity($post,$this->request->data);
    //         if ($this->Posts->save($post)) {
    //             $this->Flash->success('Add Success');
    //             return $this->redirect(['action' => 'index']);
    //         } else {
    //             $this->Flash->error('Add Error');
    //             //error
    //         }
    //     }
    //     $this->set('post',$post);
    // }
    // public function edit($id = null)
    // {
    //     $post = $this->Posts->get($id);
    //     if ($this->request->is(['post', 'patch', 'put'])) {
    //         $post = $this->Posts->patchEntity($post, $this->request->data);
    //         if ($this->Posts->save($post)) {
    //             $this->Flash->success('Edit Success');
    //             return $this->redirect(['action' => 'index']);
    //         } else {
    //             //error
    //             $this->Flash->error('Edit Error');
    //         }
    //     }
    //     $this->set(compact('post'));
    //
    // }
    // public function delete($id = null)
    // {
    //     $this->request->allowMethod(['post','delete']);
    //     $post = $this->Posts->get($id);
    //     if ($this->Posts->delete($post)) {
    //         $this->Flash->success('Delete Success');
    //     } else {
    //         $this->Flash->error('Delete Error');
    //     }
    //     return $this->redirect(['action' => 'index']);
    // }
}
